import type TESLAParser from './TESLAParser'
import type TESLAReporter from './TESLAReporter'

export interface IMarker {
  code?: string
  resource?: string
  startColumn: number
  endColumn: number
  startLineNumber: number
  endLineNumber: number
  message: string
  severity: MarkerSeverity
  source: string
  owner: string
}

export enum MarkerSeverity {
  Hint = 1,
  Info = 2,
  Warning = 4,
  Error = 8,
}

export interface IRule {
  id: string
  description: string
  link?: string
  init(parser: TESLAParser, reporter: TESLAReporter, options: unknown): void
}

export interface IRuleset {
  // HTML Rules
  'alt-require'?: boolean
  'anchor-target-blank'?: boolean
  'attr-lowercase'?: boolean
  'attr-no-duplication'?: boolean
  'attr-no-unnecessary-whitespace'?: boolean
  'attr-unsafe-chars'?: boolean
  'attr-value-double-quotes'?: boolean
  'attr-value-not-empty'?: boolean
  'attr-whitespace'?: boolean
  'bgcolor-hex'?: boolean
  'colors-hex-inline'?: boolean
  'colors-hex-style'?: boolean
  'doctype-email'?: boolean
  'doctype-first'?: boolean
  'href-protocol'?: boolean
  'img-src-not-empty'?: boolean
  'img-src-protocol'?: boolean
  'img-width-require'?: boolean
  'img-width-unit'?: boolean
  'inline-script-disabled'?: boolean
  'meta-tags'?: boolean
  'no-code-after-body'?: boolean
  'style-external-disabled'?: boolean
  'style-head'?: boolean
  'tag-pair'?: boolean
  'tagname-lowercase'?: boolean
  'tags-disabled'?: boolean
  'units-px-inline'?: boolean
  'units-px-style'?: boolean
  // Data Rules
  'data-no-unnecessary-whitespace'?: boolean
  'statement-no-cond-assign-operator'?: boolean
  'statement-endstatement-required'?: boolean
  'statement-no-invalid-tags'?: boolean
  'data-no-invalid-filters'?: boolean
  'data-string-double-quote'?: boolean
}

export interface IAttribute {
  name: string
  value: string
  quote: string
  index: number
  raw: string
}

export interface IFilter {
  name: string
  value?: string
  delimiter?: string
  quote: string
  index: number
  raw: string
}

export interface IBlock {
  tagName: string
  attributes: IAttribute[]
  filters?: IFilter[]
  conditions?: ICondition[]
  type: string
  raw: string
  position: number
  startColumn: number
  startLineNumber: number
  content: string
  long: boolean
  close: string
  lastEvent?: Partial<IBlock>
}

export interface ICondition {
  logicOperator?: string
  condition: string
  operator: string
  quote: string
  value: string
  index: number
  raw: string
}

export type Listener = (event: IBlock) => void

export interface IMapAttrs {
  [name: string]: string
}
