import type { IMarker, IRule, IRuleset, MarkerSeverity } from './types'

export default class TESLAReporter {
  public html: string
  public ruleset: IRuleset
  public messages: IMarker[]

  public constructor(html: string, ruleset: IRuleset) {
    this.html = html

    this.ruleset = ruleset
    this.messages = []
  }

  public info(
    message: string,
    startCol: number,
    endCol: number,
    rule: IRule,
    raw: string,
    line: number,
    endLine?: number,
  ): void {
    this.report(2, message, startCol, endCol, rule, raw, line, endLine)
  }

  public warn(
    message: string,
    startCol: number,
    endCol: number,
    rule: IRule,
    raw: string,
    line: number,
    endLine?: number,
  ): void {
    this.report(4, message, startCol, endCol, rule, raw, line, endLine)
  }

  public error(
    message: string,
    startCol: number,
    endCol: number,
    rule: IRule,
    raw: string,
    line: number,
    endLine?: number,
  ): void {
    this.report(8, message, startCol, endCol, rule, raw, line, endLine)
  }

  private report(
    type: MarkerSeverity,
    message: string,
    startCol: number,
    endCol: number,
    rule: IRule,
    raw: string,
    line: number,
    endLine?: number,
  ) {
    this.messages.push({
      severity: type,
      message,
      code: raw,
      resource: rule.link ? rule.link : `https://totalexpert.com/docs/user-guide/rules/${rule.id}`,
      startLineNumber: line,
      endLineNumber: endLine ?? line,
      startColumn: startCol,
      endColumn: endCol,
      source: rule.id,
      owner: 'teslahtml-validator',
    })
  }
}
