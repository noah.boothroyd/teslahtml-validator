export const disabledTagList = [
  {
    name: 'base',
    void: true,
    severity: 'warning',
    reason: '"base" tags are poorly supported in most email clients.',
  },
  {
    name: 'article',
    severity: 'warning',
    reason: '"article" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'section',
    severity: 'warning',
    reason: '"section" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'nav',
    severity: 'warning',
    reason: '"nav" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'aside',
    severity: 'warning',
    reason: '"aside" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'header',
    severity: 'warning',
    reason: '"header" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'footer',
    severity: 'warning',
    reason: '"footer" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'pre',
    severity: 'info',
    reason: '"pre" tags are poorly supported in most email clients.',
  },
  {
    name: 'figure',
    severity: 'warning',
    reason: '"figure" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'figcaption',
    severity: 'warning',
    reason: '"figcaption" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'main',
    severity: 'warning',
    reason: '"main" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'div',
    severity: 'warning',
    reason: '"div" tags are poorly supported in Outlook - use tables to create layouts for compatibility.',
  },
  {
    name: 'time',
    severity: 'warning',
    reason: '"time" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'var',
    severity: 'error',
    reason: '"var" tags are poorly supported in most email clients.',
  },
  {
    name: 'kbd',
    severity: 'warning',
    reason: '"kbd" tags are poorly supported in most email clients.',
  },
  {
    name: 'mark',
    severity: 'warning',
    reason: '"mark" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'bdi',
    severity: 'warning',
    reason: '"bdi" tags are poorly supported in most email clients.',
  },
  {
    name: 'bdo',
    severity: 'warning',
    reason: '"bdo" tags are poorly supported in most email clients.',
  },
  {
    name: 'wbr',
    void: true,
    severity: 'warning',
    reason: '"wbr" tags are poorly supported in most email clients.',
  },
  {
    name: 'ins',
    severity: 'warning',
    reason: '"ins" tags are poorly supported in most email clients',
  },
  {
    name: 'picture',
    severity: 'warning',
    reason: '"picture" tags are poorly supported in most email clients. Use .png or .img files for images.',
  },
  {
    name: 'iframe',
    severity: 'error',
    reason: '"iframe" tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'embed',
    void: true,
    severity: 'error',
    reason: '"embed" tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'object',
    severity: 'error',
    reason: '"object" tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'param',
    void: true,
    severity: 'error',
    reason: '"param" tags are deprecated and unsupported by most email clients.',
  },
  {
    name: 'video',
    severity: 'error',
    reason: '"video" tags are unsupported supported by most email clients.',
  },
  {
    name: 'audio',
    severity: 'error',
    reason: '"audio" tags are unsupported by most email clients.',
  },
  {
    name: 'source',
    void: true,
    severity: 'error',
    reason: '"source" tags are unsupported by most email clients.',
  },
  {
    name: 'track',
    void: true,
    severity: 'error',
    reason: '"track" tags are unsupported by most email clients.',
  },
  {
    name: 'form',
    severity: 'error',
    reason: '"form" tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'label',
    severity: 'error',
    reason: '"label" tags and other form tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'input',
    void: true,
    severity: 'error',
    reason: '"input" tags and other form tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'select',
    severity: 'error',
    reason: '"select" tags and other form tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'datalist',
    severity: 'error',
    reason: '"datalist" tags and other form tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'optgroup',
    severity: 'error',
    reason: '"optgroup" tags and other form tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'option',
    severity: 'error',
    reason: '"option" tags and other form tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'textarea',
    severity: 'error',
    reason: '"textarea" tags and other form tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'output',
    severity: 'warning',
    reason: '"output" tags are poorly supported by most email clients.',
  },
  {
    name: 'progress',
    severity: 'error',
    reason: '"progress" tags are unsupported by most email clients.',
  },
  {
    name: 'meter',
    severity: 'error',
    reason: '"meter" tags are unsupported by most email clients.',
  },
  {
    name: 'fieldset',
    severity: 'error',
    reason: '"fieldset" tags and other form tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'legend',
    severity: 'error',
    reason: '"legend" tags and other form tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'details',
    severity: 'warning',
    reason: '"details" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'summary',
    severity: 'warning',
    reason: '"summary" tags and other HTML5 semantics are poorly supported in most email clients.',
  },
  {
    name: 'dialog',
    severity: 'error',
    reason: '"dialog" tags are unsupported by most email clients.',
  },
  {
    name: 'script',
    severity: 'error',
    reason: '"script" tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'noscript',
    severity: 'error',
    reason: '"noscript" tags are incompatible with email clients and represent a security risk.',
  },
  {
    name: 'template',
    severity: 'error',
    reason: '"template" tags are unsupported by most email clients.',
  },
  {
    name: 'canvas',
    severity: 'error',
    reason: '"canvas" tags are incompatible with most email clients. Use .png or .img files for images.',
  },
  {
    name: 'svg',
    severity: 'error',
    reason: '"svg" tags are incompatible with most email clients. Use .png or .img files for images.',
  },
]
