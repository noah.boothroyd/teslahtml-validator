import { describe, expect, it } from 'vitest'
import { TESLAValidator } from '../main'

describe('Data Rules', () => {
  const dataHTML = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width">
      <title></title>
      <style></style>
    </head>
    <body style="">
      {% if sender.f_name = "FirstName" %}
        <p>{{ sender.f_name }}</p>
      {% elseif sender.f_name == "Other Name" %}
        <p>{{ sender.l_name }}</p>
      {% elsif sender.f_name == 'Third Name' %}
        <p>{{ sender.l_name }}</p>
      {% else %}
        <p>Nothing</p>
      {% endunless %}
      {% assign name = "FirstName" %}
      {%   if    whitespace.test ==    "toomuch" |   abs %}
      {{ too.much.whitespace  |  split: ','   }}
      {% endif    %}
      {% assign invalid = "FirstName" | invalid | test: 'invalid string' %}
      {{ filter.test | invalidfilter: 0 }}
      {% asign test_var = "text" %}
      {% if sender.f_name in names %}
      {% endif %}
      <!-- prevent Gmail on iOS font size manipulation -->
      <div style="display:none; white-space:nowrap; font:15px courier; line-height:0"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
    </body>
  </html>`
  const validationResults = TESLAValidator.verify(dataHTML, TESLAValidator.DataRuleset)
  it('data-no-unnecessary-whitespace', () => {
    expect(validationResults).toContainEqual(
      {
        severity: 8,
        message: 'Items within data brackets should be separated by only one space and not have leading/trailing whitespace',
        code: '{%   if    whitespace.test ==    "toomuch" |   abs %}',
        resource: 'https://totalexpert.com/docs/user-guide/rules/data-no-unnecessary-whitespace',
        startLineNumber: 20,
        endLineNumber: 20,
        startColumn: 7,
        endColumn: 60,
        source: 'data-no-unnecessary-whitespace',
        owner: 'teslahtml-validator',
      },
      {
        severity: 8,
        message: 'Items within data brackets should be separated by only one space and not have leading/trailing whitespace',
        code: '{{ too.much.whitespace  |  split: ","   }}',
        resource: 'https://totalexpert.com/docs/user-guide/rules/data-no-unnecessary-whitespace',
        startLineNumber: 21,
        endLineNumber: 21,
        startColumn: 7,
        endColumn: 49,
        source: 'data-no-unnecessary-whitespace',
        owner: 'teslahtml-validator',
      },
      {
        severity: 8,
        message: 'Items within data brackets should be separated by only one space and not have leading/trailing whitespace',
        code: '{% endif    %}',
        resource: 'https://totalexpert.com/docs/user-guide/rules/data-no-unnecessary-whitespace',
        startLineNumber: 22,
        endLineNumber: 22,
        startColumn: 7,
        endColumn: 21,
        source: 'data-no-unnecessary-whitespace',
        owner: 'teslahtml-validator',
      },
    )
  })
  it('data-no-invalid-filters', () => {
    expect(validationResults).toContainEqual(
      {
        severity: 8,
        message: '\'invalid\' is not a valid TESLA filter.',
        code: '| invalid ',
        resource: 'https://totalexpert.com/docs/user-guide/rules/data-no-invalid-filters',
        startLineNumber: 23,
        endLineNumber: 23,
        startColumn: 39,
        endColumn: 48,
        source: 'data-no-invalid-filters',
        owner: 'teslahtml-validator',
      },
      {
        severity: 8,
        message: '\'invalidfilter\' is not a valid TESLA filter.',
        code: '| invalidfilter: 0 ',
        resource: 'https://totalexpert.com/docs/user-guide/rules/data-no-invalid-filters',
        startLineNumber: 24,
        endLineNumber: 24,
        startColumn: 22,
        endColumn: 40,
        source: 'data-no-invalid-filters',
        owner: 'teslahtml-validator',
      },
    )
  })
  it('data-string-double-quote', () => {
    expect(validationResults).toContainEqual(
      {
        severity: 8,
        message: 'Strings in conditions must be enclosed in double quotes.',
        code: 'sender.f_name == \'Third Name\'',
        resource: 'https://totalexpert.com/docs/user-guide/rules/data-string-double-quote',
        startLineNumber: 14,
        endLineNumber: 14,
        startColumn: 16,
        endColumn: 45,
        source: 'data-string-double-quote',
        owner: 'teslahtml-validator',
      },
      {
        severity: 8,
        message: 'Strings in filters must be enclosed in double quotes.',
        code: '|  split: \',\'',
        resource: 'https://totalexpert.com/docs/user-guide/rules/data-string-double-quote',
        startLineNumber: 21,
        endLineNumber: 21,
        startColumn: 32,
        endColumn: 45,
        source: 'data-string-double-quote',
        owner: 'teslahtml-validator',
      },
      {
        severity: 8,
        message: 'Strings in filters must be enclosed in double quotes.',
        code: '| test: \'invalid string\'',
        resource: 'https://totalexpert.com/docs/user-guide/rules/data-string-double-quote',
        startLineNumber: 23,
        endLineNumber: 23,
        startColumn: 50,
        endColumn: 74,
        source: 'data-string-double-quote',
        owner: 'teslahtml-validator',
      },
    )
  })
  it('statement-no-cond-assign-operator', () => {
    expect(validationResults).toContainEqual(
      {
        severity: 8,
        message: '\'=\' is an assignment operator. Use \'==\' for \'equals\' or \'!=\' for \'does not equal\'',
        code: 'sender.f_name = "FirstName" ',
        resource: 'https://totalexpert.com/docs/user-guide/rules/statement-no-cond-assign-operator',
        startLineNumber: 10,
        endLineNumber: 10,
        startColumn: 13,
        endColumn: 39,
        source: 'statement-no-cond-assign-operator',
        owner: 'teslahtml-validator',
      },
    )
  })
  it('statement-endstatement-required', () => {
    expect(validationResults).toContainEqual(
      {
        severity: 8,
        message: 'Statements must be paired. Missing start statement for `{% endunless %}`',
        code: '{% endunless %}',
        resource: 'https://totalexpert.com/docs/user-guide/rules/statement-endstatement-required',
        startLineNumber: 18,
        endLineNumber: 18,
        startColumn: 7,
        endColumn: 22,
        source: 'statement-endstatement-required',
        owner: 'teslahtml-validator',
      },
      {
        severity: 8,
        message: 'Statements must be paired. Missing statement: `{% endif %}`. Open statement match failed `{% if sender.f_name = "FirstName" %}` on line 10.',
        code: '{% if sender.f_name = "FirstName" %}',
        resource: 'https://totalexpert.com/docs/user-guide/rules/statement-endstatement-required',
        startLineNumber: 10,
        endLineNumber: 10,
        startColumn: 7,
        endColumn: 43,
        source: 'statement-endstatement-required',
        owner: 'teslahtml-validator',
      },
    )
  })
  it('statement-no-invalid-tags', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: '\'asign\' is not a valid TESLA tag.',
      code: '{% asign test_var = "text" %}',
      resource: 'https://totalexpert.com/docs/user-guide/rules/statement-no-invalid-tags',
      startLineNumber: 25,
      endLineNumber: 25,
      startColumn: 10,
      endColumn: 15,
      source: 'statement-no-invalid-tags',
      owner: 'teslahtml-validator',
    })
  })
})
