import { describe, expect, it } from 'vitest'
import { TESLAValidator } from '../main'

describe('Email HTML Rules', () => {
  const emailHTML = `<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="">
    <head>
      <title></title>
      <link rel="stylesheet" href='mystyle.css'>
    </head>
    <body bgcolor="rgb(1,2,3)" Style="color: rgb(1,2,3);">
      <Script>more evil javascript</script>
      <style>
        .black {
          color: cmyk(0%,0%,0%,0%);
          width: 500em;
        }
      </style>
      <canvas></canvas>
      <svg></svg>
      <h1 data-parsed>Header
      Header 2</h2>
      <p class="�" style="color: hsl(1,2%,3%); width: 500pt;"   class="text-sm">Stuff</p>
      <img src="javascript:externalScript" />
      <iframe>
      </iframe>
      <a onmouseover="evilscript" href = "www.google.com">Link</a>
      <img src="space.jpg" alt="space" width="500" />
      <img src="" width="500px" alt="empty" />
      <a href="http://link.com" target="blank">Proper Link</a>
    </body>
  </html>
  <span>text</span>`
  const validationResults = TESLAValidator.verify(emailHTML, TESLAValidator.emailHTMLRuleset)
  it('alt-require', () => {
    expect(validationResults).toContainEqual({
      severity: 2,
      message: 'An alt attribute should be present on <img> elements.',
      code: '<img src="javascript:externalScript" />',
      resource: 'https://totalexpert.com/docs/user-guide/rules/alt-require',
      startLineNumber: 19,
      endLineNumber: 19,
      startColumn: 7,
      endColumn: 46,
      source: 'alt-require',
      owner: 'teslahtml-validator',
    })
  })
  it('anchor-target-blank', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: 'Anchor tags must have a \'target\' attribute with a value of \'_blank\'',
      code: '<a onmouseover="evilscript" href = "www.google.com">',
      resource: 'https://totalexpert.com/docs/user-guide/rules/anchor-target-blank',
      startLineNumber: 22,
      endLineNumber: 22,
      startColumn: 7,
      endColumn: 59,
      source: 'anchor-target-blank',
      owner: 'teslahtml-validator',
    },
    {
      severity: 8,
      message: 'The \'target\' attribute of an anchor tag must have a value of \'_blank\'',
      code: '<a href="http://link.com" target="blank">',
      resource: 'https://totalexpert.com/docs/user-guide/rules/anchor-target-blank',
      startLineNumber: 25,
      endLineNumber: 25,
      startColumn: 33,
      endColumn: 47,
      source: 'anchor-target-blank',
      owner: 'teslahtml-validator',
    })
  })
  it('attr-lowercase', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: 'The attribute name \'Style\' must be in lowercase',
      code: 'Style="color: rgb(1,2,3);"',
      resource: 'https://totalexpert.com/docs/user-guide/rules/attr-lowercase',
      startLineNumber: 6,
      endLineNumber: 6,
      startColumn: 32,
      endColumn: 58,
      source: 'attr-lowercase',
      owner: 'teslahtml-validator',
    })
  })
  it('attr-no-duplication', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: 'Duplicate attribute \'class\' found.',
      code: 'class="text-sm"',
      resource: 'https://totalexpert.com/docs/user-guide/rules/attr-no-duplication',
      startLineNumber: 18,
      endLineNumber: 18,
      startColumn: 63,
      endColumn: 80,
      source: 'attr-no-duplication',
      owner: 'teslahtml-validator',
    })
  })
  it('attr-no-unnecessary-whitespace', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: 'The attribute \'href\' must not have spaces between the name and value.',
      code: 'href = "www.google.com"',
      resource: 'https://totalexpert.com/docs/user-guide/rules/attr-no-unnecessary-whitespace',
      startLineNumber: 22,
      endLineNumber: 22,
      startColumn: 35,
      endColumn: 58,
      source: 'attr-no-unnecessary-whitespace',
      owner: 'teslahtml-validator',
    })
  })
  it('attr-unsafe-chars', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: 'The value of attribute [ class ] cannot contain an unsafe character [ \\uFFFD ].',
      code: 'class="�"',
      resource: 'https://totalexpert.com/docs/user-guide/rules/attr-unsafe-chars',
      startLineNumber: 18,
      endLineNumber: 18,
      startColumn: 10,
      endColumn: 19,
      source: 'attr-unsafe-chars',
      owner: 'teslahtml-validator',
    })
  })
  it('attr-value-double-quotes', () => {
    expect(validationResults).toContainEqual({
      severity: 4,
      message: 'The value of attribute \'href\' must be in double quotes.',
      code: 'href=\'mystyle.css\'',
      resource: 'https://totalexpert.com/docs/user-guide/rules/attr-value-double-quotes',
      startLineNumber: 4,
      endLineNumber: 4,
      startColumn: 30,
      endColumn: 48,
      source: 'attr-value-double-quotes',
      owner: 'teslahtml-validator',
    })
  })
  it('attr-value-not-empty', () => {
    expect(validationResults).toContainEqual({
      severity: 4,
      message: 'The attribute \'data-parsed\' must have a value.',
      code: 'data-parsed',
      resource: 'https://totalexpert.com/docs/user-guide/rules/attr-value-not-empty',
      startLineNumber: 16,
      endLineNumber: 16,
      startColumn: 11,
      endColumn: 22,
      source: 'attr-value-not-empty',
      owner: 'teslahtml-validator',
    })
  })
  it('attr-whitespace', () => {
    expect(validationResults).toContainEqual({
      severity: 4,
      message: 'The attribute \'class\' should be separated by only one space.',
      code: 'class="text-sm"',
      resource: 'https://totalexpert.com/docs/user-guide/rules/attr-whitespace',
      startLineNumber: 18,
      endLineNumber: 18,
      startColumn: 62,
      endColumn: 80,
      source: 'attr-whitespace',
      owner: 'teslahtml-validator',
    })
  })
  it('bgcolor-hex', () => {
    expect(validationResults).toContainEqual({
      severity: 2,
      message: 'The attribute \'bgcolor\' should have the color value in a six digit hex format.',
      code: 'bgcolor="rgb(1,2,3)"',
      resource: 'https://totalexpert.com/docs/user-guide/rules/bgcolor-hex',
      startLineNumber: 6,
      endLineNumber: 6,
      startColumn: 11,
      endColumn: 31,
      source: 'bgcolor-hex',
      owner: 'teslahtml-validator',
    })
  })
  it('colors-hex-inline', () => {
    expect(validationResults).toContainEqual({
      severity: 4,
      message: 'Non-hex color codes are poorly supported. Setting colors in 6 digit hex format is recommended.',
      code: 'rgb(1,2,3)',
      resource: 'https://totalexpert.com/docs/user-guide/rules/colors-hex-inline',
      startLineNumber: 6,
      endLineNumber: 6,
      startColumn: 46,
      endColumn: 56,
      source: 'colors-hex-inline',
      owner: 'teslahtml-validator',
    },
    {
      severity: 4,
      message: 'Non-hex color codes are poorly supported. Setting colors in 6 digit hex format is recommended.',
      code: 'hsl(1,2%,3%)',
      resource: 'https://totalexpert.com/docs/user-guide/rules/colors-hex-inline',
      startLineNumber: 18,
      endLineNumber: 18,
      startColumn: 34,
      endColumn: 46,
      source: 'colors-hex-inline',
      owner: 'teslahtml-validator',
    })
  })
  it('colors-hex-style', () => {
    expect(validationResults).toContainEqual({
      severity: 4,
      message: 'Non-hex color codes are poorly supported. Setting colors in 6 digit hex format is recommended.',
      code: 'color: cmyk(0%,0%,0%,0%);',
      resource: 'https://totalexpert.com/docs/user-guide/rules/colors-hex-style',
      startLineNumber: 10,
      endLineNumber: 10,
      startColumn: 11,
      endColumn: 36,
      source: 'colors-hex-style',
      owner: 'teslahtml-validator',
    })
  })
  it('doctype-email', () => {
    const doctypeHTML = `<!DOCTYPE html>\n${emailHTML}`
    const doctypeResults = TESLAValidator.verify(doctypeHTML)
    expect(doctypeResults).toContainEqual({
      severity: 4,
      message: 'Poorly supported doctype. The recommended doctype for email is XHTML 1.0 Transitional',
      code: '<!DOCTYPE html>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/doctype-email',
      startLineNumber: 1,
      endLineNumber: 1,
      startColumn: 1,
      endColumn: 16,
      source: 'doctype-email',
      owner: 'teslahtml-validator',
    })
  })
  it('doctype-first', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: 'Doctype must be declared first.',
      code: '<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="">',
      resource: 'https://totalexpert.com/docs/user-guide/rules/doctype-first',
      startLineNumber: 1,
      endLineNumber: 1,
      startColumn: 1,
      endColumn: 77,
      source: 'doctype-first',
      owner: 'teslahtml-validator',
    })
  })
  it('href-protocol', () => {
    expect(validationResults).toContainEqual({
      severity: 4,
      message: 'URL value of the href attribute \'www.google.com\' should contain either the \'http://\' or \'https://\' protocol',
      code: 'href = "www.google.com"',
      resource: 'https://totalexpert.com/docs/user-guide/rules/href-protocol',
      startLineNumber: 22,
      endLineNumber: 22,
      startColumn: 35,
      endColumn: 58,
      source: 'href-protocol',
      owner: 'teslahtml-validator',
    })
  })
  it('img-src-not-empty', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: 'The src attribute of an image must have a value',
      code: 'src=""',
      resource: 'https://totalexpert.com/docs/user-guide/rules/img-src-not-empty',
      startLineNumber: 24,
      endLineNumber: 24,
      startColumn: 12,
      endColumn: 18,
      source: 'img-src-not-empty',
      owner: 'teslahtml-validator',
    })
  })
  it('img-src-protocol', () => {
    expect(validationResults).toContainEqual({
      severity: 4,
      message: 'URL value of the image src attribute \'space.jpg\' should contain either the \'http://\' or \'https://\' protocol',
      code: 'src="space.jpg"',
      resource: 'https://totalexpert.com/docs/user-guide/rules/img-src-protocol',
      startLineNumber: 23,
      endLineNumber: 23,
      startColumn: 12,
      endColumn: 27,
      source: 'img-src-protocol',
      owner: 'teslahtml-validator',
    })
  })
  it('img-width-require', () => {
    expect(validationResults).toContainEqual({
      severity: 4,
      message: 'A width attribute should be present on images for Outlook compatibility',
      code: '<img src="javascript:externalScript" />',
      resource: 'https://totalexpert.com/docs/user-guide/rules/img-width-require',
      startLineNumber: 19,
      endLineNumber: 19,
      startColumn: 7,
      endColumn: 46,
      source: 'img-width-require',
      owner: 'teslahtml-validator',
    })
  })
  it('img-width-unit', () => {
    expect(validationResults).toContainEqual({
      severity: 2,
      message: 'The width attribute of image tags should only contain digits. Font unit is already set to px.',
      code: 'width="500px"',
      resource: 'https://totalexpert.com/docs/user-guide/rules/img-width-unit',
      startLineNumber: 24,
      endLineNumber: 24,
      startColumn: 19,
      endColumn: 32,
      source: 'img-width-unit',
      owner: 'teslahtml-validator',
    })
  })
  it('inline-script-disabled', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: 'Using scripts as sources for images or links is not allowed in email.',
      code: ' src="javascript:externalScript"',
      resource: 'https://totalexpert.com/docs/user-guide/rules/inline-script-disabled',
      startLineNumber: 19,
      endLineNumber: 19,
      startColumn: 12,
      endColumn: 43,
      source: 'inline-script-disabled',
      owner: 'teslahtml-validator',
    },
    {
      severity: 8,
      message: 'Inline script listeners such as \'onmouseover\' are not allowed in email.',
      code: 'onmouseover="evilscript"',
      resource: 'https://totalexpert.com/docs/user-guide/rules/inline-script-disabled',
      startLineNumber: 21,
      endLineNumber: 21,
      startColumn: 10,
      endColumn: 34,
      source: 'inline-script-disabled',
      owner: 'teslahtml-validator',
    })
  })
  it('meta-tags', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: 'A meta tag containing character encoding information is required in the head tags.',
      code: '<head> ... </head>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/meta-tags',
      startLineNumber: 2,
      endLineNumber: 5,
      startColumn: 5,
      endColumn: 12,
      source: 'meta-tags',
      owner: 'teslahtml-validator',
    },
    {
      severity: 8,
      message: 'A meta tag containing viewport information should be included in the head tags for mobile styles to work.',
      code: '<head> ... </head>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/meta-tags',
      startLineNumber: 2,
      endLineNumber: 5,
      startColumn: 5,
      endColumn: 12,
      source: 'meta-tags',
      owner: 'teslahtml-validator',
    })
  })
  it('no-code-after-body', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: 'No code is allowed after the closing </body> tag except for the </html> tag.',
      code: '<span> ... </span>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/no-code-after-body',
      startLineNumber: 28,
      endLineNumber: 28,
      startColumn: 3,
      endColumn: 20,
      source: 'no-code-after-body',
      owner: 'teslahtml-validator',
    })
  })
  it('style-external-disabled', () => {
    expect(validationResults).toContainEqual({
      severity: 4,
      message: 'External stylesheets are poorly supported in email. Use inline styles instead.',
      code: '<link rel="stylesheet" href=\'mystyle.css\'>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/style-external-disabled',
      startLineNumber: 4,
      endLineNumber: 4,
      startColumn: 7,
      endColumn: 49,
      source: 'style-external-disabled',
      owner: 'teslahtml-validator',
    })
  })
  it('style-head', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: '\'<style>\' tags must go inside the \'<head>\' tags for Gmail compatibility',
      code: '<style> ... </style>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/style-head',
      startLineNumber: 8,
      endLineNumber: 13,
      startColumn: 7,
      endColumn: 15,
      source: 'style-head',
      owner: 'teslahtml-validator',
    })
  })
  it('tag-pair', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: 'Tags must be paired. Missing start tag: \'</h2>\'',
      code: '</h2>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/tag-pair',
      startLineNumber: 17,
      endLineNumber: 17,
      startColumn: 15,
      endColumn: 20,
      source: 'tag-pair',
      owner: 'teslahtml-validator',
    },
    {
      severity: 8,
      message: 'Tags must be paired. Missing tag: \'</h1>\'. Start tag match failed [<h1 data-parsed>] on line 16.',
      code: '</body>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/tag-pair',
      startLineNumber: 16,
      endLineNumber: 16,
      startColumn: 7,
      endColumn: 23,
      source: 'tag-pair',
      owner: 'teslahtml-validator',
    })
  })
  it('tagname-lowercase', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: 'The html element name \'Script\' must be in lowercase.',
      code: '<Script>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/tagname-lowercase',
      startLineNumber: 7,
      endLineNumber: 7,
      startColumn: 8,
      endColumn: 14,
      source: 'tagname-lowercase',
      owner: 'teslahtml-validator',
    })
  })
  it('tags-disabled', () => {
    expect(validationResults).toContainEqual({
      severity: 8,
      message: '`script` tags are incompatible with email clients and represent a security risk.',
      code: '<Script> ... </script>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/tags-disabled',
      startLineNumber: 7,
      endLineNumber: 7,
      startColumn: 7,
      endColumn: 44,
      source: 'tags-disabled',
      owner: 'teslahtml-validator',
    },
    {
      severity: 8,
      message: '`canvas` tags are incompatible with most email clients. Use .png or .img files for images.',
      code: '<canvas> ... </canvas>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/tags-disabled',
      startLineNumber: 14,
      endLineNumber: 14,
      startColumn: 7,
      endColumn: 24,
      source: 'tags-disabled',
      owner: 'teslahtml-validator',
    },
    {
      severity: 8,
      message: '`svg` tags are incompatible with most email clients. Use .png or .img files for images.',
      code: '<svg> ... </svg>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/tags-disabled',
      startLineNumber: 15,
      endLineNumber: 15,
      startColumn: 7,
      endColumn: 18,
      source: 'tags-disabled',
      owner: 'teslahtml-validator',
    },
    {
      severity: 8,
      message: '`iframe` tags are incompatible with email clients and represent a security risk.',
      code: '<iframe> ... </iframe>',
      resource: 'https://totalexpert.com/docs/user-guide/rules/tags-disabled',
      startLineNumber: 20,
      endLineNumber: 21,
      startColumn: 7,
      endColumn: 16,
      source: 'tags-disabled',
      owner: 'teslahtml-validator',
    })
  })
  it('units-px-inline', () => {
    expect(validationResults).toContainEqual({
      severity: 2,
      message: 'Non-px units are poorly supported in email. Use absolute px values.',
      code: '500pt',
      resource: 'https://totalexpert.com/docs/user-guide/rules/units-px-inline',
      startLineNumber: 18,
      endLineNumber: 18,
      startColumn: 55,
      endColumn: 60,
      source: 'units-px-inline',
      owner: 'teslahtml-validator',
    })
  })
  it('units-px-style', () => {
    expect(validationResults).toContainEqual({
      severity: 2,
      message: 'Non-px units are poorly supported in email. Use absolute px values.',
      code: 'width: 500em;',
      resource: 'https://totalexpert.com/docs/user-guide/rules/units-px-style',
      startLineNumber: 11,
      endLineNumber: 11,
      startColumn: 11,
      endColumn: 24,
      source: 'units-px-style',
      owner: 'teslahtml-validator',
    })
  })
})
