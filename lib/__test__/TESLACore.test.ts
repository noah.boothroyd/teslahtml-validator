import { expect, test } from 'vitest'
import { TESLAValidator } from '../main'
import { altRequire, anchorTargetBlank } from '../rules/html'

test('addRule', () => {
  TESLAValidator.rules = {}
  TESLAValidator.addRule(altRequire)
  TESLAValidator.addRule(anchorTargetBlank)
  expect(TESLAValidator.rules).toEqual({
    'alt-require': {
      id: 'alt-require',
      description: 'The alt attribute of an <img> element should be present. Many users of email clients have images turned off first, so an informative alt attribute helps mitigate any confusion on first view.',
      init: expect.any(Function),
    },
    'anchor-target-blank': {
      id: 'anchor-target-blank',
      description: 'Anchor tags must have a target="_blank" attribute for Outlook compatibility. There is a risk of links opening in the Outlook sidebar otherwise.',
      init: expect.any(Function),
    },
  })
})

