import TESLAParser from './TESLAParser'
import TESLAReporter from './TESLAReporter'
import * as HTMLRules from './rules/html'
import * as DataRules from './rules/tesla'
import type { IRule, IRuleset } from './types'

class TELSAValidatorCore {
  public rules: { [id: string]: IRule } = {}
  public readonly emailHTMLRuleset: IRuleset = {
    'alt-require': true,
    'anchor-target-blank': true,
    'attr-lowercase': true,
    'attr-no-duplication': true,
    'attr-no-unnecessary-whitespace': true,
    'attr-unsafe-chars': true,
    'attr-value-double-quotes': true,
    'attr-value-not-empty': true,
    'attr-whitespace': true,
    'bgcolor-hex': true,
    'colors-hex-inline': true,
    'colors-hex-style': true,
    'doctype-email': true,
    'doctype-first': true,
    'href-protocol': true,
    'img-src-not-empty': true,
    'img-src-protocol': true,
    'img-width-require': true,
    'img-width-unit': true,
    'inline-script-disabled': true,
    'meta-tags': true,
    'no-code-after-body': true,
    'style-external-disabled': true,
    'style-head': true,
    'tag-pair': true,
    'tagname-lowercase': true,
    'tags-disabled': true,
    'units-px-inline': true,
    'units-px-style': true,
  }

  public readonly DataRuleset: IRuleset = {
    'statement-no-cond-assign-operator': true,
    'statement-endstatement-required': true,
    'statement-no-invalid-tags': true,
    'data-no-unnecessary-whitespace': true,
    'data-no-invalid-filters': true,
    'data-string-double-quote': true,
  }

  public addRule(rule: IRule) {
    this.rules[rule.id] = rule
  }

  public verify(html: string, ruleset: IRuleset = this.emailHTMLRuleset) {
    if (Object.keys(ruleset).length === 0)
      ruleset = this.emailHTMLRuleset

    const parser = new TESLAParser()
    const reporter = new TESLAReporter(html, ruleset)
    const rules = this.rules
    let rule: IRule
    for (const id in ruleset) {
      rule = rules[id]
      if (rule !== undefined && ruleset[id as keyof IRuleset] !== false)
        rule.init(parser, reporter, ruleset[id as keyof IRuleset])
    }

    parser.parse(html)
    return reporter.messages
  }
}

export const TESLAValidator = new TELSAValidatorCore()

Object.keys(HTMLRules).forEach((key) => {
  TESLAValidator.addRule(HTMLRules[key as keyof typeof HTMLRules])
})
Object.keys(DataRules).forEach((key) => {
  TESLAValidator.addRule(DataRules[key as keyof typeof DataRules])
})

export { HTMLRules, DataRules, TESLAParser, TESLAReporter }
