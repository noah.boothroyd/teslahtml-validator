/* eslint-disable no-cond-assign */
import type { IAttribute, IBlock, ICondition, IFilter, IMapAttrs, Listener } from './types'

export default class TESLAParser {
  public lastEvent: Partial<IBlock> | null

  private _listeners: { [type: string]: Listener[] }
  private _arrBlocks: Array<Partial<IBlock>>

  public constructor() {
    this._listeners = {}
    this._arrBlocks = []
    this.lastEvent = null
  }

  public makeMap(str: string): {
    [key: string]: boolean
  } {
    const obj: { [key: string]: boolean } = {}
    const items = str.split(',')

    for (let i = 0; i < items.length; i++)
      obj[items[i]] = true

    return obj
  }

  public parse(html: string): void {
    // new hotness: /(?:(?<statement>{%\s*(?<statement_tag>\w+)\s*(?<statement_condition>((?!%}|\|).)*)(?<statement_filters>\|((?!%}).)*)?%}))|(?:(?<output>{{(.*?)(?<output_filters>\|.*?)?}}))/g
    const regTag
      // eslint-disable-next-line no-control-regex
      = /<(?:\/([^\s>]+)\s*|!--([\s\S]*?)--|!([^>]*?)|([\w\-:]+)((?:\s+[^\s"'>\/=\x00-\x0F\x7F\x80-\x9F]+(?:\s*=\s*(?:"[^"]*"|'[^']*'|[^\s"'>]*))?)*?)\s*(\/?))>|(?:({%\s*(\w+)\s*((?:(?!%}|\|).)*)(\|(?:(?!%}).)*)?%}))|(?:({{(.*?)(\|.*?)?}}))/g
    const regAttr
      // eslint-disable-next-line no-control-regex
      = /\s*([^\s"'>\/=\x00-\x0F\x7F\x80-\x9F]+)(?:\s*=\s*(?:(")([^"]*)"|(')([^']*)'|([^\s"'>]*)))?/g
    const regFilter = /(\|) *([\w]*)(:)? *(?:("|')?([^!=><"'|\r\n\t\f\v]*)("|')?)?/g
    const regCondition = /( *(?:and|or) *)?((?:(?! (?:and|or|contains|in) )[^!=><"'\r\n\t\f\v])+) *(?:([!=><]+|contains|in) *("|')?((?:(?! (?:and|or|contains|in) )[^!=><"'\r\n\t\f\v])+)('|")?)? */g
    const regAssignCondition = /((?:[^!=><"'\r\n\t\f\v])+)(?:([!=><]+|in|contains) *("|')?((?:[^!=><"'\r\n\t\f\v])+)("|')?)? */g
    const regLine = /\r?\n/g

    let match: RegExpExecArray | null
    let matchIndex: number
    let lastIndex = 0
    let tagName: string
    let arrAttrs: IAttribute[]
    let arrFilters: IFilter[]
    let arrConditions: ICondition[]
    let text: string
    let lastLineIndex = 0
    let line = 1
    const arrBlocks = this._arrBlocks

    this.fire('start', {
      position: 0,
      startLineNumber: 1,
      startColumn: 1,
    })

    const saveBlock = (
      type: string,
      raw: string,
      position: number,
      data?: Partial<IBlock>,
    ) => {
      const startCol = position - lastLineIndex + 1
      if (data === undefined)
        data = {}
      data.raw = raw
      data.position = position
      data.startLineNumber = line
      data.startColumn = startCol
      arrBlocks.push(data)
      this.fire(type, data)

      while (regLine.exec(raw)) {
        line++
        lastLineIndex = position + regLine.lastIndex
      }
    }

    while ((match = regTag.exec(html))) {
      matchIndex = match.index
      if (matchIndex > lastIndex) {
        text = html.substring(lastIndex, matchIndex)
        saveBlock('text', text, lastIndex)
      }
      lastIndex = regTag.lastIndex

      if ((tagName = match[1])) {
        // End of label
        saveBlock('tagend', match[0], matchIndex, {
          tagName,
        })
        continue
      }

      if ((tagName = match[4])) {
        // Label start
        arrAttrs = []
        const attrs = match[5]
        let attrMatch
        let attrMatchCount = 0

        while ((attrMatch = regAttr.exec(attrs))) {
          const name = attrMatch[1]
          const quote = attrMatch[2]
            ? attrMatch[2]
            : attrMatch[4]
              ? attrMatch[4]
              : ''
          const value = attrMatch[3]
            ? attrMatch[3]
            : attrMatch[5]
              ? attrMatch[5]
              : attrMatch[6]
                ? attrMatch[6]
                : ''

          arrAttrs.push({
            name,
            value,
            quote,
            index: attrMatch.index,
            raw: attrMatch[0],
          })
          attrMatchCount += attrMatch[0].length
        }
        if (attrMatchCount === attrs.length) {
          saveBlock('tagstart', match[0], matchIndex, {
            tagName,
            attributes: arrAttrs,
            close: match[6],
          })
        }
        else {
          // If a miss match occurs, the current content is matched to text
          saveBlock('text', match[0], matchIndex)
        }
      }
      else if (match[2] || match[3]) {
        // Comment tag
        saveBlock('comment', match[0], matchIndex, {
          content: match[2] || match[3],
          long: !!match[2],
        })
      }
      // Data Output Blocks
      if (match[11]) {
        const data: Partial<IBlock> = {
          content: match[12],
        }
        // Collect Filters
        arrFilters = []
        if (match[13]) {
          const filters = match[13]
          let filterMatch
          while ((filterMatch = regFilter.exec(filters))) {
            const name = filterMatch[2]
            const quote = filterMatch[4] && filterMatch[6]
              ? filterMatch[4] === filterMatch[6]
                ? filterMatch[4]
                : 'invalid'
              : filterMatch[4] || filterMatch[6]
                ? 'invalid'
                : ''
            const value = filterMatch[5] ? filterMatch[5] : undefined
            const delimiter = filterMatch[3] ? filterMatch[3] : undefined
            arrFilters.push({
              name,
              value,
              delimiter,
              quote,
              index: filterMatch.index,
              raw: filterMatch[0],
            })
          }
        }
        data.filters = arrFilters
        saveBlock('outputTag', match[11], matchIndex, data)
      }
      // Data Statement Blocks
      if ((tagName = match[8])) {
        const data: Partial<IBlock> = {
          tagName,
        }
        if (/\bend[\w]+\b/.test(tagName)) {
          saveBlock('endStatementTag', match[7], matchIndex, data)
        }
        else {
          // Collect Conditions
          arrConditions = []
          if (match[9]) {
            const conditions = match[9]
            let conditionMatch
            if (tagName === 'assign') {
              while ((conditionMatch = regAssignCondition.exec(conditions))) {
                const condition = conditionMatch[1]
                const operator = conditionMatch[2] ? conditionMatch[2] : ''
                const quote = conditionMatch[3] && conditionMatch[5]
                  ? conditionMatch[3] === conditionMatch[5]
                    ? conditionMatch[3]
                    : 'invalid'
                  : conditionMatch[3] || conditionMatch[3]
                    ? 'invalid'
                    : ''
                const value = conditionMatch[4] ? conditionMatch[4] : ''
                arrConditions.push({
                  condition,
                  operator,
                  quote,
                  value,
                  index: conditionMatch.index,
                  raw: conditionMatch[0],
                })
              }
            }
            else {
              while ((conditionMatch = regCondition.exec(conditions))) {
                const logicOperator = conditionMatch[1] ? conditionMatch[1] : ''
                const condition = conditionMatch[2]
                const operator = conditionMatch[3] ? conditionMatch[3] : ''
                const quote = conditionMatch[4] && conditionMatch[6]
                  ? conditionMatch[4] === conditionMatch[6]
                    ? conditionMatch[4]
                    : 'invalid'
                  : conditionMatch[4] || conditionMatch[6]
                    ? 'invalid'
                    : ''
                const value = conditionMatch[5] ? conditionMatch[5] : ''
                arrConditions.push({
                  logicOperator,
                  condition,
                  operator,
                  quote,
                  value,
                  index: conditionMatch.index,
                  raw: conditionMatch[0],
                })
              }
            }
          }
          data.conditions = arrConditions
          // Collect Filters
          arrFilters = []
          if (match[10]) {
            const filters = match[10]
            let filterMatch
            while ((filterMatch = regFilter.exec(filters))) {
              const name = filterMatch[2]
              const quote = filterMatch[4] && filterMatch[6]
                ? filterMatch[4] === filterMatch[6]
                  ? filterMatch[4]
                  : 'invalid'
                : filterMatch[4] || filterMatch[6]
                  ? 'invalid'
                  : ''
              const value = filterMatch[5] ? filterMatch[5] : undefined
              const delimiter = filterMatch[3] ? filterMatch[3] : undefined
              arrFilters.push({
                name,
                value,
                delimiter,
                quote,
                index: filterMatch.index,
                raw: filterMatch[0],
              })
            }
          }
          data.filters = arrFilters
          saveBlock('startStatementTag', match[7], matchIndex, data)
        }
      }
    }
    if (html.length > lastIndex) {
      // End text
      text = html.substring(lastIndex, html.length)
      saveBlock('text', text, lastIndex)
    }

    this.fire('end', {
      position: lastIndex,
      startLineNumber: line,
      startColumn: html.length - lastLineIndex + 1,
    })
    // console.log(arrBlocks)
  }

  public addListener(types: string, listener: Listener): void {
    const _listeners = this._listeners
    const arrTypes = types.split(/[,\s]/)
    let type

    for (let i = 0, l = arrTypes.length; i < l; i++) {
      type = arrTypes[i]
      if (_listeners[type] === undefined)
        _listeners[type] = []

      _listeners[type].push(listener)
    }
  }

  public fire(type: string, data?: Partial<IBlock>): void {
    if (data === undefined)
      data = {}

    data.type = type

    let listeners: Listener[] = []
    const listenersType = this._listeners[type]
    const listenersAll = this._listeners.all

    if (listenersType !== undefined)
      listeners = listeners.concat(listenersType)

    if (listenersAll !== undefined)
      listeners = listeners.concat(listenersAll)

    const lastEvent = this.lastEvent
    if (lastEvent !== null) {
      delete lastEvent.lastEvent
      data.lastEvent = lastEvent
    }

    this.lastEvent = data

    for (let i = 0, l = listeners.length; i < l; i++) {
      // @ts-expect-error TODO: we may improve where data is actually a Block or a Partial<Block>
      listeners[i].call(this, data)
    }
  }

  public removeListener(type: string, listener: Listener): void {
    const listenersType: Listener[] | undefined = this._listeners[type]
    if (listenersType !== undefined) {
      for (let i = 0, l = listenersType.length; i < l; i++) {
        if (listenersType[i] === listener) {
          listenersType.splice(i, 1)
          break
        }
      }
    }
  }

  public getMapAttrs(arrAttrs: IAttribute[]): IMapAttrs {
    const mapAttrs: { [name: string]: string } = {}
    let attr: IAttribute

    for (let i = 0, l = arrAttrs.length; i < l; i++) {
      attr = arrAttrs[i]
      mapAttrs[attr.name] = attr.value
    }

    return mapAttrs
  }
}
