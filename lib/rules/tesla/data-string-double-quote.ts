// ERROR: Strings should be enclosed in double quotes.
import type { IRule } from '../../types'

export default {
  id: 'data-string-double-quote',
  description: 'Strings in conditions must be enclosed in double quotes.',
  init(parser, reporter) {
    parser.addListener('startStatementTag', (event) => {
      const conditions = event.conditions
      const filters = event.filters
      if (conditions && conditions.length > 0) {
        conditions.forEach((condition) => {
          const startCol = event.startColumn + 3 + event.tagName.length + condition.index
          if (condition.quote === 'invalid' || condition.quote === '\'') {
            reporter.error(
              'Strings in conditions must be enclosed in double quotes.',
              startCol + 1,
              startCol + condition.raw.length,
              this,
              condition.raw.trim(),
              event.startLineNumber,
            )
          }
        })
      }
      if (filters && filters.length > 0) {
        filters.forEach((filter) => {
          const preFilter = event.raw.substring(0, event.raw.indexOf('|'))
          const startCol = event.startColumn + preFilter.length + 1 + filter.index
          if (filter.quote === 'invalid' || filter.quote === '\'') {
            reporter.error(
              'Strings in filters must be enclosed in double quotes.',
              startCol,
              startCol + filter.raw.trim().length,
              this,
              filter.raw.trim(),
              event.startLineNumber,
            )
          }
        })
      }
    })

    parser.addListener('outputTag', (event) => {
      const filters = event.filters
      if (filters && filters.length > 0) {
        filters.forEach((filter) => {
          const preFilter = event.raw.substring(0, event.raw.indexOf('|'))
          const startCol = event.startColumn + preFilter.length + 1 + filter.index
          if (filter.quote === 'invalid' || filter.quote === '\'') {
            reporter.error(
              'Strings in filters must be enclosed in double quotes.',
              startCol,
              startCol + filter.raw.trim().length,
              this,
              filter.raw.trim(),
              event.startLineNumber,
            )
          }
        })
      }
    })
  },
} as IRule
