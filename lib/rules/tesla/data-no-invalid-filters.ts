// ERROR: Only filters from the built in list of filters can be used.
import type { IRule } from '../../types'

const builtInLiquidFilters = ['abs', 'append', 'at_least', 'at_most', 'capitalize', 'ceil', 'compact', 'date', 'default', 'divided_by', 'downcase', 'escape', 'escape_once', 'first', 'floor', 'join', 'json', 'last', 'lstrip', 'map', 'minus', 'modulo', 'newline_to_br', 'plus', 'prepend', 'remove', 'remove_first', 'replace', 'replace_first', 'reverse', 'round', 'rstrip', 'size', 'slice', 'sort', 'sort_natural', 'split', 'strip', 'strip_html', 'strip_newlines', 'times', 'truncate', 'truncatewords', 'uniq', 'upcase', 'url_decode', 'url_encode', 'where']

export default {
  id: 'data-no-invalid-filters',
  description: 'Only filters from the built-in list of filters can be used.',
  init(parser, reporter) {
    parser.addListener('startStatementTag', (event) => {
      const filters = event.filters
      if (filters && filters.length > 0) {
        filters.forEach((filter) => {
          if (!builtInLiquidFilters.includes(filter.name)) {
            const preFilter = event.raw.substring(0, event.raw.indexOf('|'))
            const startCol = event.startColumn + preFilter.length + filter.index
            reporter.error(
              `\'${filter.name}\' is not a valid TESLA filter.`,
              startCol,
              startCol + filter.raw.trim().length,
              this,
              filter.raw,
              event.startLineNumber,
            )
          }
        })
      }
    })
    parser.addListener('outputTag', (event) => {
      const filters = event.filters
      if (filters && filters.length > 0) {
        filters.forEach((filter) => {
          if (!builtInLiquidFilters.includes(filter.name)) {
            const preFilter = event.raw.substring(0, event.raw.indexOf('|'))
            const startCol = event.startColumn + preFilter.length + filter.index
            reporter.error(
              `\'${filter.name}\' is not a valid TESLA filter.`,
              startCol,
              startCol + filter.raw.trim().length,
              this,
              filter.raw,
              event.startLineNumber,
            )
          }
        })
      }
    })
  },
} as IRule
