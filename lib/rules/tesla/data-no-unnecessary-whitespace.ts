// ERROR: Items within statement brackets should be separated by only one space and not have leading/trailing whitespace.
import type { IRule } from '../../types'
const checkWhitespace = (html: string): boolean => {
  const strippedString = html.replace(/\s+/g, ' ').trim()
  return html === strippedString
}

export default {
  id: 'data-no-unnecessary-whitespace',
  description: 'Items within data brackets should be separated by only one space and not have leading/trailing whitespace.',
  init(parser, reporter) {
    parser.addListener('startStatementTag', (event) => {
      const raw = event.raw
      const hasNoWhitespace = checkWhitespace(raw)
      if (!hasNoWhitespace) {
        reporter.error(
          'Items within data brackets should be separated by only one space and not have leading/trailing whitespace',
          event.startColumn,
          event.startColumn + event.raw.length,
          this,
          raw,
          event.startLineNumber,
        )
      }
    })
    parser.addListener('endStatementTag', (event) => {
      const raw = event.raw
      const hasNoWhitespace = checkWhitespace(raw)
      if (!hasNoWhitespace) {
        reporter.error(
          'Items within data brackets should be separated by only one space and not have leading/trailing whitespace',
          event.startColumn,
          event.startColumn + event.raw.length,
          this,
          raw,
          event.startLineNumber,
        )
      }
    })
    parser.addListener('outputTag', (event) => {
      const raw = event.raw
      const hasNoWhitespace = checkWhitespace(raw)
      if (!hasNoWhitespace) {
        reporter.error(
          'Items within data brackets should be separated by only one space and not have leading/trailing whitespace',
          event.startColumn,
          event.startColumn + event.raw.length,
          this,
          raw,
          event.startLineNumber,
        )
      }
    })
  },
} as IRule
