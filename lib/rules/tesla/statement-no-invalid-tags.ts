// ERROR: Only tags from the built in list of tags can be used.
import type { IRule } from '../../types'

export const builtInLiquidTags = ['if', 'else', 'elseif', 'elsif', 'endif', 'render', 'assign', 'capture', 'endcapture', 'case', 'endcase', 'comment', 'endcomment', 'cycle', 'decrement', 'for', 'endfor', 'include', 'increment', 'layout', 'raw', 'endraw', 'render', 'tablerow', 'endtablerow', 'unless', 'endunless']

export default {
  id: 'statement-no-invalid-tags',
  description: 'Only tags from the built-in list of tags can be used.',
  init(parser, reporter) {
    parser.addListener('startStatementTag', (event) => {
      const tagName = event.tagName.toLowerCase()
      if (!builtInLiquidTags.includes(tagName)) {
        const startCol = event.startColumn + 3
        reporter.error(
          `\'${tagName}\' is not a valid TESLA tag.`,
          startCol,
          startCol + tagName.length,
          this,
          event.raw,
          event.startLineNumber,
        )
      }
    })
  },
} as IRule
