// ERROR: '=' is an assignment operator. Use '==' for 'equals' or '!=' for 'does not equal',
import type { IRule } from '../../types'

export default {
  id: 'statement-no-cond-assign-operator',
  description: '\'=\' is an assignment operator. Use \'==\' for \'equals\' or \'!=\' for \'does not equal\'',
  init(parser, reporter) {
    const logicTags = ['if', 'unless', 'elsif', 'elseif']
    parser.addListener('startStatementTag', (event) => {
      const tagName = event.tagName.toLowerCase()
      if (logicTags.includes(tagName)) {
        const conditions = event.conditions
        conditions?.forEach((condition) => {
          const startCol = event.startColumn + event.tagName.length + 3 + condition.index
          if (condition.operator === '=') {
            reporter.error(
              '\'=\' is an assignment operator. Use \'==\' for \'equals\' or \'!=\' for \'does not equal\'',
              startCol + 1,
              startCol + condition.raw.trimEnd().length,
              this,
              condition.raw,
              event.startLineNumber,
            )
          }
        })
      }
    })
  },
} as IRule
