// ERROR: '{% endif %}' is required at the end of {% if %} statements.
import type { IBlock, IRule } from '../../types'

export default {
  id: 'statement-endstatement-required',
  description: '{% endstatement %} is required at the end of {% if/unless/case/for/raw/comment/capture %} statements.',
  init(parser, reporter) {
    const stack: Array<Partial<IBlock>> = []
    const tagsEndRequired = ['if', 'capture', 'case', 'comment', 'for', 'raw', 'tablerow', 'unless']
    parser.addListener('startStatementTag', (event) => {
      const tagName = event.tagName.toLowerCase()
      if (tagsEndRequired.includes(tagName)) {
        stack.push({
          tagName,
          startLineNumber: event.startLineNumber,
          startColumn: event.startColumn,
          raw: event.raw,
        })
      }
    })

    parser.addListener('endStatementTag', (event) => {
      const endTagName = event.tagName.toLowerCase()
      const startTagName = endTagName.replace('end', '')
      // Look up the matching start tag
      let position
      for (position = stack.length - 1; position >= 0; position--) {
        if (stack[position].tagName === startTagName)
          break
      }

      if (position >= 0) {
        const statementArray = []
        for (let i = stack.length - 1; i > position; i--) {
          const stackTag = stack[i].tagName
          const endTagName = stackTag ? stackTag.replace(stackTag, `end${stackTag}`) : ''
          statementArray.push(`{% ${endTagName} %}`)
        }

        if (statementArray.length > 0) {
          const lastEvent = stack[stack.length - 1]
          const startCol = lastEvent.startColumn ? lastEvent.startColumn : 1
          const endCol = lastEvent.raw ? lastEvent.raw.length : event.raw.length
          reporter.error(
            `Statements must be paired. Missing end statement: \`${statementArray.join('')}\`. Start statement match failed \`${lastEvent.raw}\` on line ${lastEvent.startLineNumber}.`,
            startCol,
            startCol + endCol,
            this,
            event.raw.trim(),
            lastEvent.startLineNumber ? lastEvent.startLineNumber : event.startLineNumber,
          )
        }
        stack.length = position
      }
      else {
        reporter.error(
          `Statements must be paired. Missing start statement for \`${event.raw}\``,
          event.startColumn,
          event.startColumn + event.raw.length,
          this,
          event.raw.trim(),
          event.startLineNumber,
        )
      }
    })

    parser.addListener('end', (event) => {
      const statementArray = []
      for (let i = stack.length - 1; i >= 0; i--) {
        const stackTag = stack[i].tagName
        const endTagName = stackTag ? stackTag.replace(stackTag, `end${stackTag}`) : ''
        statementArray.push(`{% ${endTagName} %}`)
      }
      if (statementArray.length > 0) {
        const lastEvent = stack[stack.length - 1]
        const startCol = lastEvent.startColumn ? lastEvent.startColumn : 1
        const endCol = lastEvent.raw ? lastEvent.raw.length : event.raw.length
        reporter.error(
          `Statements must be paired. Missing statement: \`${statementArray.join('')}\`. Open statement match failed \`${lastEvent.raw}\` on line ${lastEvent.startLineNumber}.`,
          startCol,
          startCol + endCol,
          this,
          lastEvent.raw ? lastEvent.raw : '',
          lastEvent.startLineNumber ? lastEvent.startLineNumber : event.startLineNumber,
        )
      }
    })
  },
} as IRule
