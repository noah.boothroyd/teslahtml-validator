// WARN: An image src attribute must have an absolute link with either http:// or https:// protocol
import type { IRule } from '../../types'

export default {
  id: 'img-src-protocol',
  description: 'The src attribute of an image must have an absolute link with either http:// or https:// protocol',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const tagName = event.tagName.toLowerCase()
      const attributes = event.attributes

      for (let i = 0, l = attributes.length; i < l; i++) {
        const attr = attributes[i]
        if (tagName === 'img' && (attr.name === 'src' && attr.value !== '')) {
          if (/^https?:\/\//.test(attr.value) === false && /^\s*javascript:/i.test(attr.value) === false) {
            const startCol = event.startColumn + event.tagName.length + 1 + attr.index
            reporter.warn(
              `URL value of the image src attribute \'${attr.value}\' should contain either the \'http://\' or \'https://\' protocol`,
              startCol + 1,
              startCol + attr.raw.length,
              this,
              attr.raw.trim(),
              event.startLineNumber,
            )
          }
          break
        }
      }
    })
  },
} as IRule
