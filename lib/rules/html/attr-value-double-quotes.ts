// WARN: Attribute values must be in double quotes.
import type { IRule } from '../../types'

export default {
  id: 'attr-value-double-quotes',
  description: 'Attribute values should be in double quotes.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attributes = event.attributes
      attributes.forEach((attr) => {
        const startCol = event.startColumn + event.tagName.length + 1 + attr.index
        if (
          (attr.value !== '' && attr.quote !== '"')
          || (attr.value === '' && attr.quote === '\'')
        ) {
          reporter.warn(
            `The value of attribute \'${attr.name}\' must be in double quotes.`,
            startCol + 1,
            startCol + attr.raw.length,
            this,
            attr.raw.trim(),
            event.startLineNumber,
          )
        }
      })
    })
  },
} as IRule
