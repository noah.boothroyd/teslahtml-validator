// INFO: Non-px units are poorly supported in email. Use absolute px values.
import type { IRule } from '../../types'

const invalidLengthUnitRegex = /\d+(?:[.,]\d+)?(cm|mm|in|pt|em|rem|vw|vh)/g

export default {
  id: 'units-px-style',
  description: 'Non-px units are poorly supported in email. Use absolute px values.',
  init(parser, reporter) {
    let isInStyle = false

    parser.addListener('tagstart', (event) => {
      const tagName = event.tagName.toLowerCase()
      if (tagName === 'style')
        isInStyle = true
    })

    parser.addListener('tagend', (event) => {
      const tagName = event.tagName.toLowerCase()
      if (tagName === 'style')
        isInStyle = false
    })

    parser.addListener('text', (event) => {
      if (isInStyle) {
        const styleLines = event.raw.split(/\r?\n/)
        const impactedLines = styleLines.filter(line => invalidLengthUnitRegex.test(line))
        if (impactedLines.length > 0) {
          impactedLines.forEach((line) => {
            const impactedIndex = styleLines.findIndex(item => item === line)
            const startCol = line.length - line.trimStart().length + 1
            reporter.info(
              'Non-px units are poorly supported in email. Use absolute px values.',
              startCol,
              startCol + line.trim().length,
              this,
              line.trim(),
              event.startLineNumber + impactedIndex,
            )
          })
        }
      }
    })
  },
} as IRule
