import type { IRule } from '../../types'

// INFO: The alt attribute of an <img> element must be present
export default {
  id: 'alt-require',
  description: 'The alt attribute of an <img> element should be present. Many users of email clients have images turned off first, so an informative alt attribute helps mitigate any confusion on first view.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const tagName = event.tagName.toLowerCase()
      const mapAttrs = parser.getMapAttrs(event.attributes)

      if (tagName === 'img' && !('alt' in mapAttrs)) {
        reporter.info(
          'An alt attribute should be present on <img> elements.',
          event.startColumn,
          event.startColumn + event.raw.length,
          this,
          event.raw.trim(),
          event.startLineNumber,
        )
      }
    })
  },
} as IRule
