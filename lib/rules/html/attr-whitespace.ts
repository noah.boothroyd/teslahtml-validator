// WARN: All attributes should be separated by only one space and not have leading/trailing whitespace.
import type { IRule } from '../../types'

export default {
  id: 'attr-whitespace',
  description: 'All attributes should be separated by only one space and not have leading/trailing whitespace.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attributes = event.attributes
      attributes.forEach((attr) => {
        const startCol = event.startColumn + event.tagName.length + 1 + attr.index
        // Check first and last characters for spaces
        if (` ${attr.raw.trim()}` !== attr.raw) {
          reporter.warn(
            `The attribute \'${attr.name}\' should be separated by only one space.`,
            startCol,
            startCol + attr.raw.length,
            this,
            attr.raw.trim(),
            event.startLineNumber,
          )
        }
      })
    })
  },
} as IRule
