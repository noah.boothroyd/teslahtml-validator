// WARN: Non-hex color codes are poorly supported. Setting colors in 6 digit hex format is recommended.
import type { IRule } from '../../types'

const rgbRegex = /rgb\((\d{1,3}),\s?(\d{1,3}),\s?(\d{1,3})\)/
const hslRegex = /hsl\((\d+),\s?(\d+)%,\s?(\d+)%\)/
const cmykRegex = /cmyk\((\d+)%,\s?(\d+)%,\s?(\d+)%,\s?(\d+)%\)/

export default {
  id: 'colors-hex-style',
  description: 'Non-hex color codes are poorly supported. Setting colors in 6 digit hex format is recommended.',
  init(parser, reporter) {
    let isInStyle = false

    parser.addListener('tagstart', (event) => {
      const tagName = event.tagName.toLowerCase()
      if (tagName === 'style')
        isInStyle = true
    })

    parser.addListener('tagend', (event) => {
      const tagName = event.tagName.toLowerCase()
      if (tagName === 'style')
        isInStyle = false
    })

    parser.addListener('text', (event) => {
      if (isInStyle) {
        const styleLines = event.raw.split(/\r?\n/)
        const impactedLines = styleLines.filter(line => rgbRegex.test(line) || hslRegex.test(line) || cmykRegex.test(line))
        if (impactedLines.length > 0) {
          impactedLines.forEach((line) => {
            const impactedIndex = styleLines.findIndex(item => item === line)
            const startCol = line.length - line.trimStart().length + 1
            reporter.warn(
              'Non-hex color codes are poorly supported. Setting colors in 6 digit hex format is recommended.',
              startCol,
              startCol + line.trim().length,
              this,
              line.trim(),
              event.startLineNumber + impactedIndex,
            )
          })
        }
      }
    })
  },
} as IRule
