// ERROR: <style> tags must go inside the <head></head> tags for Gmail compatibility.
import type { IRule, Listener } from '../../types'

export default {
  id: 'style-head',
  description: '<style> tags must go inside the <head></head> tags for Gmail compatibility.',
  init(parser, reporter) {
    let isInHead = false
    let tagStartLine: number
    let tagStartCol: number
    let tagStartCode: string
    const onTagStart: Listener = (event) => {
      const tagName = event.tagName.toLowerCase()
      if (tagName === 'head')
        isInHead = true
      if (isInHead === false && tagName === 'style') {
        tagStartLine = event.startLineNumber
        tagStartCol = event.startColumn
        tagStartCode = event.raw
      }
    }
    const onTagEnd: Listener = (event) => {
      const tagName = event.tagName.toLowerCase()
      if (tagName === 'head')
        isInHead = false
      if (isInHead === false && tagName === 'style') {
        reporter.error(
          '\'<style>\' tags must go inside the \'<head>\' tags for Gmail compatibility',
          tagStartCol,
          event.startColumn + event.raw.length,
          this,
          `${tagStartCode} ... ${event.raw}`,
          tagStartLine,
          event.startLineNumber,
        )
      }
    }

    parser.addListener('tagstart', onTagStart)
    parser.addListener('tagend', onTagEnd)
  },
} as IRule
