// ERROR: Tag must be paired.
import type { IBlock, IRule } from '../../types'

export default {
  id: 'tag-pair',
  description: 'Tag must be paired.',
  init(parser, reporter) {
    const stack: Array<Partial<IBlock>> = []
    const mapEmptyTags = parser.makeMap('br,hr,img,link,meta')

    parser.addListener('tagstart', (event) => {
      const tagName = event.tagName.toLowerCase()
      if (mapEmptyTags[tagName] === undefined && !event.close) {
        stack.push({
          tagName,
          startLineNumber: event.startLineNumber,
          startColumn: event.startColumn,
          raw: event.raw,
        })
      }
    })

    parser.addListener('tagend', (event) => {
      const tagName = event.tagName.toLowerCase()
      // Look up the matching start tag
      let position
      for (position = stack.length - 1; position >= 0; position--) {
        if (stack[position].tagName === tagName)
          break
      }

      if (position >= 0) {
        const arrTags = []
        for (let i = stack.length - 1; i > position; i--)
          arrTags.push(`</${stack[i].tagName}>`)

        if (arrTags.length > 0) {
          const lastEvent = stack[stack.length - 1]
          const startCol = lastEvent.startColumn ? lastEvent.startColumn : 1
          const endCol = lastEvent.raw ? lastEvent.raw.length : event.raw.length
          reporter.error(
            `Tags must be paired. Missing tag: \'${arrTags.join(
              '',
            )}\'. Start tag match failed [${lastEvent.raw}] on line ${
              lastEvent.startLineNumber
            }.`,
            startCol,
            startCol + endCol,
            this,
            event.raw.trim(),
            lastEvent.startLineNumber ? lastEvent.startLineNumber : event.startLineNumber,
          )
        }
        stack.length = position
      }
      else {
        reporter.error(
          `Tags must be paired. Missing start tag: \'${event.raw}\'`,
          event.startColumn,
          event.startColumn + event.raw.length,
          this,
          event.raw.trim(),
          event.startLineNumber,
        )
      }
    })

    parser.addListener('end', (event) => {
      const arrTags = []

      for (let i = stack.length - 1; i >= 0; i--)
        arrTags.push(`</${stack[i].tagName}>`)

      if (arrTags.length > 0) {
        const lastEvent = stack[stack.length - 1]
        const startCol = lastEvent.startColumn ? lastEvent.startColumn : 1
        const endCol = lastEvent.raw ? lastEvent.raw.length : event.raw.length
        reporter.error(
          `Tags must be paired. Missing tag: \'${arrTags.join(
            '',
          )}\'. Open tag match failed [${lastEvent.raw}] on line ${
            lastEvent.startLineNumber
          }.`,
          startCol,
          startCol + endCol,
          this,
          event.raw,
          lastEvent.startLineNumber ? lastEvent.startLineNumber : event.startLineNumber,
        )
      }
    })
  },
} as IRule
