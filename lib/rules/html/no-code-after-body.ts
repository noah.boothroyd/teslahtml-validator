// ERROR: No code is allowed after the closing </body> tag except for the </html> tag.
import type { IBlock, IRule } from '../../types'

export default {
  id: 'no-code-after-body',
  description: 'No code is allowed after the closing </body> tag except for the </html> tag.',
  init(parser, reporter) {
    let isAfterBody = false
    const stack: Array<Partial<IBlock>> = []

    parser.addListener('tagend', (event) => {
      const tagName = event.tagName.toLowerCase()
      if (tagName === 'body')
        isAfterBody = true
    })

    parser.addListener('tagstart,tagend', (event) => {
      const tagName = event.tagName.toLowerCase()
      if (isAfterBody === true) {
        stack.push({
          tagName,
          startLineNumber: event.startLineNumber,
          startColumn: event.startColumn,
          attributes: event.attributes,
          raw: event.raw,
        })
      }
    })

    parser.addListener('end', (event) => {
      const filteredStack = stack.filter(block => block.tagName !== 'html' && block.tagName !== 'body')
      if (filteredStack.length > 1) {
        const lastIndex = filteredStack.length - 1
        const startCol = filteredStack[0].startColumn ? filteredStack[0].startColumn : event.startColumn
        const endCol = event.lastEvent && (event.lastEvent.startColumn && event.lastEvent.raw) ? event.lastEvent.startColumn + event.lastEvent.raw.length : startCol + 1
        reporter.error(
          'No code is allowed after the closing </body> tag except for the </html> tag.',
          startCol,
          endCol,
          this,
          `${filteredStack[0].raw} ... ${lastIndex !== 0 ? filteredStack[lastIndex].raw : ''}`,
          filteredStack[0].startLineNumber ? filteredStack[0].startLineNumber : event.startLineNumber,
          filteredStack[lastIndex].startLineNumber,
        )
      }
    })
  },
} as IRule
