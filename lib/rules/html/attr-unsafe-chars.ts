// ERROR: Attribute values cannot contain unsafe chars.
import type { IRule } from '../../types'

export default {
  id: 'attr-unsafe-chars',
  description: 'Attribute values cannot contain unsafe characters.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attributes = event.attributes
      const regUnsafe
        // eslint-disable-next-line no-misleading-character-class, no-control-regex
        = /[\u0000-\u0008\u000B\u000C\u000E-\u001F\u007F-\u009F\u00AD\u0600-\u0604\u070F\u17B4\u17B5\u200C-\u200F\u2028-\u202F\u2060-\u206F\uFEFF\uFFF0-\uFFFF]/
      attributes.forEach((attr) => {
        const match = regUnsafe.exec(attr.value)
        if (match !== null) {
          const startCol = event.startColumn + event.tagName.length + 1 + attr.index
          const unsafeCode = escape(match[0])
            .replace(/%u/, '\\u')
            .replace(/%/, '\\x')
          reporter.error(
            `The value of attribute [ ${attr.name} ] cannot contain an unsafe character [ ${unsafeCode} ].`,
            startCol + 1,
            startCol + attr.raw.length,
            this,
            attr.raw.trim(),
            event.startLineNumber,
          )
        }
      })
    })
  },
} as IRule
