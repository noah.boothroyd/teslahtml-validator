// WARN: Non-hex color codes are poorly supported. Setting colors in 6 digit hex format is recommended.
import type { IRule } from '../../types'

const rgbRegex = /rgb\((\d{1,3}),\s?(\d{1,3}),\s?(\d{1,3})\)/g
const hslRegex = /hsl\((\d+),\s?(\d+)%,\s?(\d+)%\)/g
const cmykRegex = /cmyk\((\d+)%,\s?(\d+)%,\s?(\d+)%,\s?(\d+)%\)/g

export default {
  id: 'colors-hex-inline',
  description: 'Non-hex color codes are poorly supported. Setting colors in 6 digit hex format is recommended.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attributes = event.attributes
      for (let i = 0, l = attributes.length; i < l; i++) {
        const attr = attributes[i]
        if (attr.name.toLowerCase() === 'style') {
          const startCol = event.startColumn + event.tagName.length + 1 + attr.index + 1 + attr.name.length + 2
          const rgbMatches = Array.from(attr.value.matchAll(rgbRegex))
          const hslMatches = Array.from(attr.value.matchAll(hslRegex))
          const cmykMatches = Array.from(attr.value.matchAll(cmykRegex))
          const impactedMatches = rgbMatches.concat(hslMatches, cmykMatches)
          if (impactedMatches.length > 0) {
            impactedMatches.forEach((match) => {
              reporter.warn(
                'Non-hex color codes are poorly supported. Setting colors in 6 digit hex format is recommended.',
                match.index ? startCol + match.index : startCol,
                match.index ? startCol + match.index + match[0].length : startCol + attr.value.length,
                this,
                match[0],
                event.startLineNumber,
              )
            })
          }
          break
        }
      }
    })
  },
} as IRule
