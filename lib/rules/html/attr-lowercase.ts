// ERROR: All attribute names must be in lowercase.
import type { IRule } from '../../types'

const exceptions = /^on(unload|message|submit|select|scroll|resize|mouseover|mouseout|mousemove|mouseleave|mouseenter|mousedown|load|keyup|keypress|keydown|focus|dblclick|click|change|blur|error)$/i

export default {
  id: 'attr-lowercase',
  description: 'All attribute names must be in lowercase per the XHTML standard, which is the recommended standard for email.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attributes = event.attributes
      attributes.forEach((attr) => {
        const attributeName = attr.name
        const startCol = event.startColumn + event.tagName.length + 1 + attr.index
        if (attributeName !== attributeName.toLowerCase() && exceptions.test(attributeName.toLowerCase()) === false) {
          reporter.error(
            `The attribute name \'${attributeName}\' must be in lowercase`,
            startCol + 1,
            startCol + attr.raw.length,
            this,
            attr.raw.trim(),
            event.startLineNumber,
          )
        }
      })
    })
  },
} as IRule
