// ERROR: The src attribute of an img must have a value.
import type { IRule } from '../../types'

export default {
  id: 'img-src-not-empty',
  description: 'The src attribute of an img must have a value.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const tagName = event.tagName
      const attributes = event.attributes
      for (let i = 0, l = attributes.length; i < l; i++) {
        const attr = attributes[i]
        if ((tagName === 'img' && attr.name === 'src') && attr.value === '') {
          const startCol = event.startColumn + event.tagName.length + 1 + attr.index
          reporter.error(
            `The ${attr.name} attribute of an image must have a value`,
            startCol + 1,
            startCol + attr.raw.length,
            this,
            attr.raw.trim(),
            event.startLineNumber,
          )
          break
        }
      }
    })
  },
} as IRule
