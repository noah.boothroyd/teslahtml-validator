// ERROR: <canvas> tags are not allowed.
import type { IRule, Listener } from '../../types'
import { disabledTagList } from '../../data/disabledTags'

export default {
  id: 'tags-disabled',
  description: 'Certain tags are not allowed in email because they are very poorly supported across many email clients.',
  init(parser, reporter) {
    let hasDisabledStartTag = false
    let tagStartLine: number
    let tagStartCol: number
    let tagStartCode: string

    const onTagStart: Listener = (event) => {
      const eventTag = event.tagName.toLowerCase()
      const disabledTagObject = disabledTagList.find(tag => tag.name === eventTag)
      if (disabledTagObject && disabledTagObject.void === true) {
        const severity = disabledTagObject.severity
        const markerParams: [string, number, number, IRule, string, number, number?] = [
          disabledTagObject.reason,
          event.startColumn,
          event.startColumn + event.raw.length,
          this,
          event.raw,
          event.startLineNumber,
        ]
        switch (severity) {
          case 'info':
            reporter.info(...markerParams)
            break
          case 'warning':
            reporter.warn(...markerParams)
            break
          case 'error':
            reporter.error(...markerParams)
            break
        }
      }
      else if (disabledTagObject && disabledTagObject.void === undefined) {
        hasDisabledStartTag = true
        tagStartLine = event.startLineNumber
        tagStartCol = event.startColumn
        tagStartCode = event.raw
      }
    }
    const onTagEnd: Listener = (event) => {
      const eventTag = event.tagName.toLowerCase()
      const disabledTagObject = disabledTagList.find(tag => tag.name === eventTag)
      if (disabledTagObject && hasDisabledStartTag === true) {
        const severity = disabledTagObject.severity
        const markerParams: [string, number, number, IRule, string, number, number?] = [
          disabledTagObject.reason,
          tagStartCol,
          event.startColumn + event.raw.length,
          this,
          `${tagStartCode} ... ${event.raw}`,
          tagStartLine,
          event.startLineNumber,
        ]
        switch (severity) {
          case 'info':
            reporter.info(...markerParams)
            break
          case 'warning':
            reporter.warn(...markerParams)
            break
          case 'error':
            reporter.error(...markerParams)
            break
        }
      }
    }

    parser.addListener('tagstart', onTagStart)
    parser.addListener('tagend', onTagEnd)
  },
} as IRule
