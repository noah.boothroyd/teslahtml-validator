// INFO: Width attribute is required on images for Outlook compatibility
import type { IRule } from '../../types'

const digitRegex = /^[0-9]+%?$/
export default {
  id: 'img-width-unit',
  description: 'Width attributes should not contain size units - they are already set to use px.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const tagName = event.tagName
      const attributes = event.attributes
      for (let i = 0, l = attributes.length; i < l; i++) {
        const attr = attributes[i]
        if ((tagName === 'img' && (attr.name === 'width' && attr.value !== '')) && digitRegex.test(attr.value) === false) {
          const startCol = event.startColumn + event.tagName.length + 1 + attr.index
          reporter.info(
            `The ${attr.name} attribute of image tags should only contain digits. Font unit is already set to px.`,
            startCol + 1,
            startCol + attr.raw.length,
            this,
            attr.raw.trim(),
            event.startLineNumber,
          )
          break
        }
      }
    })
  },
} as IRule
