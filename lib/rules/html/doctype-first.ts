// ERROR: Doctype must be declared first.
import type { IRule, Listener } from '../../types'

export default {
  id: 'doctype-first',
  description: 'Doctype must be declared first.',
  init(parser, reporter) {
    const allEvent: Listener = (event) => {
      if (
        event.type === 'start'
        || (event.type === 'text' && /^\s*$/.test(event.raw))
      )
        return

      if (
        (event.type !== 'comment' && event.long === false)
        || /^DOCTYPE\s+/i.test(event.content) === false
      ) {
        reporter.error(
          'Doctype must be declared first.',
          event.startColumn,
          event.startColumn + event.raw.length,
          this,
          event.raw.trim(),
          event.startLineNumber,
        )
      }

      parser.removeListener('all', allEvent)
    }
    parser.addListener('all', allEvent)
  },
} as IRule
