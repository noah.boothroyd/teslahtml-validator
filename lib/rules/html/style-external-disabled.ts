// WARN: External stylesheets are very poorly supported in email. Use inline styles instead.
import type { IRule } from '../../types'

export default {
  id: 'style-external-disabled',
  description: 'External stylesheets are very poorly supported in email. Use inline styles instead.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const tagName = event.tagName
      const mapAttrs = parser.getMapAttrs(event.attributes)
      if (tagName === 'link' && ('rel' in mapAttrs)) {
        if (mapAttrs.rel === 'stylesheet') {
          reporter.warn(
            'External stylesheets are poorly supported in email. Use inline styles instead.',
            event.startColumn,
            event.startColumn + event.raw.length,
            this,
            event.raw.trim(),
            event.startLineNumber,
          )
        }
      }
    })
  },
} as IRule
