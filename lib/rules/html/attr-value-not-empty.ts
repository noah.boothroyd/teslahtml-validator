// WARN: All attributes must have values.
import type { IRule } from '../../types'

export default {
  id: 'attr-value-not-empty',
  description: 'All attributes must have values.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attributes = event.attributes
      attributes.forEach((attr) => {
        const startCol = event.startColumn + event.tagName.length + 1 + attr.index
        if (attr.value === '' && attr.quote === '') {
          reporter.warn(
            `The attribute \'${attr.name}\' must have a value.`,
            startCol + 1,
            startCol + attr.raw.length,
            this,
            attr.raw.trim(),
            event.startLineNumber,
          )
        }
      })
    })
  },
} as IRule
