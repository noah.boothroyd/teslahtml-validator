// ERROR: Elements cannot have duplicate attributes.
import type { IRule } from '../../types'

export default {
  id: 'attr-no-duplication',
  description: 'Elements cannot have duplicate attributes.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attributes = event.attributes
      const mapAttributeName: { [name: string]: boolean } = {}
      attributes.forEach((attr) => {
        const attributeName = attr.name
        const startCol = event.startColumn + event.tagName.length + 1 + attr.index
        if (mapAttributeName[attributeName] === true) {
          reporter.error(
            `Duplicate attribute \'${attributeName}\' found.`,
            startCol + 1,
            startCol + attr.raw.length,
            this,
            attr.raw.trim(),
            event.startLineNumber,
          )
        }
        mapAttributeName[attributeName] = true
      })
    })
  },
} as IRule
