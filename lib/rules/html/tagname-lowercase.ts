// WARN: All html element names must be in lowercase.
import type { IRule } from '../../types'

export default {
  id: 'tagname-lowercase',
  description: 'All html element names must be in lowercase.',
  init(parser, reporter) {
    parser.addListener('tagstart,tagend', (event) => {
      const tagName = event.tagName
      const startCol = event.type === 'tagend' ? event.startColumn + 2 : event.startColumn + 1
      const endCol = event.type === 'tagend' ? event.startColumn + tagName.length + 2 : event.startColumn + tagName.length + 1
      if (tagName !== tagName.toLowerCase()) {
        reporter.error(
          `The html element name \'${tagName}\' must be in lowercase.`,
          startCol,
          endCol,
          this,
          event.raw.trim(),
          event.startLineNumber,
        )
      }
    })
  },
} as IRule
