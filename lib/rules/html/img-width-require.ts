// WARN: Width attribute is required on images for Outlook compatibility
import type { IRule } from '../../types'

export default {
  id: 'img-width-require',
  description: 'Width attribute is required on images for Outlook compatibility',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const tagName = event.tagName
      const mapAttrs = parser.getMapAttrs(event.attributes)
      if (tagName === 'img' && !('width' in mapAttrs)) {
        reporter.warn(
          'A width attribute should be present on images for Outlook compatibility',
          event.startColumn,
          event.startColumn + event.raw.length,
          this,
          event.raw.trim(),
          event.startLineNumber,
        )
      }
    })
  },
} as IRule
