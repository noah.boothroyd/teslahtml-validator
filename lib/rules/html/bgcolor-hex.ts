// INFO: The bgcolor attribute must have the color set in six digit hex format.
import type { IRule } from '../../types'

const supportedBgcolorTags = ['body', 'table', 'thead', 'tbody', 'tfoot', 'tr', 'td', 'th']

const hexRegex = /#([A-Fa-f0-9]{6})/

export default {
  id: 'bgcolor-hex',
  description: 'The bgcolor attribute must have the color set in six digit hex format.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const tagName = event.tagName.toLowerCase()
      const attributes = event.attributes
      attributes.forEach((attr) => {
        const startCol = event.startColumn + event.tagName.length + 1 + attr.index
        if (supportedBgcolorTags.includes(tagName) && attr.name === 'bgcolor') {
          const hexMatch = hexRegex.test(attr.value)
          if (!hexMatch) {
            reporter.info(
              'The attribute \'bgcolor\' should have the color value in a six digit hex format.',
              startCol + 1,
              startCol + attr.raw.length,
              this,
              attr.raw.trim(),
              event.startLineNumber,
            )
          }
        }
      })
    })
  },
} as IRule
