// INFO: Non-px units are poorly supported in email. Use absolute px values.
import type { IRule } from '../../types'

const invalidLengthUnitRegex = /\d+(?:[.,]\d+)?(cm|mm|in|pt|em|rem|vw|vh)/g

export default {
  id: 'units-px-inline',
  description: 'Non-px units are poorly supported in email. Use absolute px values.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attributes = event.attributes
      for (let i = 0, l = attributes.length; i < l; i++) {
        const attr = attributes[i]
        if (attr.name.toLowerCase() === 'style') {
          const startCol = event.startColumn + event.tagName.length + 1 + attr.index + 1 + attr.name.length + 2
          const impactedMatches = Array.from(attr.value.matchAll(invalidLengthUnitRegex))
          if (impactedMatches.length > 0) {
            impactedMatches.forEach((match) => {
              reporter.info(
                'Non-px units are poorly supported in email. Use absolute px values.',
                match.index ? startCol + match.index : startCol,
                match.index ? startCol + match.index + match[0].length : startCol + attr.value.length,
                this,
                match[0],
                event.startLineNumber,
              )
            })
          }

          break
        }
      }
    })
  },
} as IRule
