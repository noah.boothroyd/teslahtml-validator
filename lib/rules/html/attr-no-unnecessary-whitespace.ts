// ERROR: No spaces between attribute names and values.
import type { IRule } from '../../types'

export default {
  id: 'attr-no-unnecessary-whitespace',
  description: 'No spaces between attribute names and values.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attributes = event.attributes
      attributes.forEach((attr) => {
        const startCol = event.startColumn + event.tagName.length + 1 + attr.index
        const match = /(\s*)=(\s*)/.exec(attr.raw.trim())
        if (match && (match[1].length !== 0 || match[2].length !== 0)) {
          reporter.error(
            `The attribute \'${attr.name}\' must not have spaces between the name and value.`,
            startCol + 1,
            startCol + attr.raw.length,
            this,
            attr.raw.trim(),
            event.startLineNumber,
          )
        }
      })
    })
  },
} as IRule
