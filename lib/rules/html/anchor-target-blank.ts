// ERROR: Anchor tags must have a target="_blank" attribute for Outlook compatibility
import type { IRule } from '../../types'

export default {
  id: 'anchor-target-blank',
  description: 'Anchor tags must have a target="_blank" attribute for Outlook compatibility. There is a risk of links opening in the Outlook sidebar otherwise.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const tagName = event.tagName.toLowerCase()
      const attributes = event.attributes
      const mapAttrs = parser.getMapAttrs(event.attributes)
      const startCol = event.startColumn + tagName.length + 1
      if (tagName === 'a' && !('target' in mapAttrs)) {
        reporter.error(
          'Anchor tags must have a \'target\' attribute with a value of \'_blank\'',
          event.startColumn,
          event.startColumn + event.raw.length,
          this,
          event.raw.trim(),
          event.startLineNumber,
        )
      }
      else if (tagName === 'a' && mapAttrs.target !== '_blank') {
        const targetAttribute = attributes.find(attr => attr.name === 'target')
        const targetStartCol = targetAttribute ? startCol + targetAttribute.index + 1 : startCol
        const targetEndCol = targetAttribute ? targetStartCol + targetAttribute.raw.length - 1 : startCol + event.raw.length
        reporter.error(
          'The \'target\' attribute of an anchor tag must have a value of \'_blank\'',
          targetStartCol,
          targetEndCol,
          this,
          event.raw.trim(),
          event.startLineNumber,
        )
      }
    })
  },
} as IRule

