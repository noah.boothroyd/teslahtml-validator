// ERROR: <script> tags are not allowed
import type { IRule } from '../../types'

export default {
  id: 'inline-script-disabled',
  description: 'Inline scripting is not allowed in email.',
  init(parser, reporter) {
    parser.addListener('tagstart', (event) => {
      const attributes = event.attributes
      const reEvent
        = /^on\w+/i
      attributes.forEach((attr) => {
        const attributeName = attr.name.toLowerCase()
        if (reEvent.test(attributeName) === true) {
          const startCol = event.startColumn + event.tagName.length + 1 + attr.index
          reporter.error(
            `Inline script listeners such as \'${attr.name}\' are not allowed in email.`,
            startCol + 1,
            startCol + attr.raw.length,
            this,
            attr.raw.trim(),
            event.startLineNumber,
          )
        }
        else if (attributeName === 'src' || attributeName === 'href') {
          if (/^\s*javascript:/i.test(attr.value)) {
            const startCol = event.startColumn + event.tagName.length + 1 + attr.index
            reporter.error(
              'Using scripts as sources for images or links is not allowed in email.',
              startCol + 1,
              startCol + attr.raw.length,
              this,
              attr.raw,
              event.startLineNumber,
            )
          }
        }
      })
    })
  },
} as IRule
