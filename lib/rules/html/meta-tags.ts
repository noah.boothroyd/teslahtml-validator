// ERROR: UTF-8 Character Encoding meta tag required
import type { IRule, Listener } from '../../types'

export default {
  id: 'meta-tags',
  description: 'UTF-8 Character Encoding and viewport metatags are required',
  init(parser, reporter) {
    let headBegin = false
    let headStartLine: number
    let headStartCol: number
    let headStartCode: string
    let hasMetaTag = false
    let hasUtf8Tag = false
    let hasViewPortTag = false

    const onTagStart: Listener = (event) => {
      const tagName = event.tagName.toLowerCase()
      if (tagName === 'head') {
        headBegin = true
        headStartCode = event.raw
        headStartCol = event.startColumn
        headStartLine = event.startLineNumber
      }
      else if (tagName === 'meta' && headBegin) {
        hasMetaTag = true
      }
      if (hasMetaTag && tagName === 'meta') {
        // Check for the right attributes
        const metaAttrs = parser.getMapAttrs(event.attributes)
        if ((metaAttrs['http-equiv']?.toLowerCase() === 'content-type' && metaAttrs.content?.toLowerCase() === 'text/html; charset=utf-8') || (metaAttrs.charset?.toLowerCase() === 'utf-8'))
          hasUtf8Tag = true

        if (metaAttrs.name?.toLowerCase() === 'viewport' && metaAttrs.content?.toLowerCase() === 'width=device-width')
          hasViewPortTag = true
      }
    }

    const onTagEnd: Listener = (event) => {
      const tagName = event.tagName.toLowerCase()
      if (tagName === 'head' && hasMetaTag === true) {
        if (hasUtf8Tag === false) {
          reporter.error(
            'A meta tag containing character encoding information is required in the head tags.',
            headStartCol,
            event.raw.length,
            this,
            `${headStartCode} ... ${event.raw}`,
            headStartLine,
            event.startLineNumber,
          )
        }
        if (hasViewPortTag === false) {
          reporter.error(
            'A meta tag containing viewport information should be included in the head tags for mobile styles to work.',
            headStartCol,
            event.raw.length,
            this,
            `${headStartCode} ... ${event.raw}`,
            headStartLine,
            event.startLineNumber,
          )
        }
        parser.removeListener('tagstart', onTagStart)
        parser.removeListener('tagend', onTagEnd)
      }
      else if (tagName === 'head' && hasMetaTag === false) {
        reporter.error(
          'A meta tag containing character encoding information is required in the head tags.',
          headStartCol,
          event.startColumn + event.raw.length,
          this,
          `${headStartCode} ... ${event.raw}`,
          headStartLine,
          event.startLineNumber,
        )
        reporter.error(
          'A meta tag containing viewport information should be included in the head tags for mobile styles to work.',
          headStartCol,
          event.startColumn + event.raw.length,
          this,
          `${headStartCode} ... ${event.raw}`,
          headStartLine,
          event.startLineNumber,
        )
        parser.removeListener('tagstart', onTagStart)
        parser.removeListener('tagend', onTagEnd)
      }
    }

    parser.addListener('tagstart', onTagStart)
    parser.addListener('tagend', onTagEnd)
  },
} as IRule
