// WARN: Poorly supported doctype. The recommended doctype for email is XHTML 1.0 Transitional.
import type { IRule, Listener } from '../../types'

export default {
  id: 'doctype-email',
  description: 'Poorly supported doctype. The recommended doctype for email is XHTML 1.0 Transitional.',
  init(parser, reporter) {
    const onComment: Listener = (event) => {
      if (event.long === false && event.content.toLowerCase() !== 'doctype html public "-//w3c//dtd xhtml 1.0 transitional//en" "http://www.w3.org/tr/xhtml1/dtd/xhtml1-transitional.dtd"') {
        reporter.warn(
          'Poorly supported doctype. The recommended doctype for email is XHTML 1.0 Transitional',
          event.startColumn,
          event.startColumn + event.raw.length,
          this,
          event.raw.trim(),
          event.startLineNumber,
        )
      }
    }
    const onTagStart: Listener = () => {
      parser.removeListener('comment', onComment)
      parser.removeListener('tagstart', onTagStart)
    }
    parser.addListener('all', onComment)
    parser.addListener('tagstart', onTagStart)
  },
} as IRule
