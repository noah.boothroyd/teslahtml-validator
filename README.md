## TESLA (Total Expert Specific Language) Validator

#### A microservice for validating HTML and the TESLA data language.

### Table of Contents
- **[Installation and Usage](#installation-and-usage)**
- **[How it Works](#how-it-works)**
- **[Writing New Rules](#writing-new-rules)**

### Installation and Usage

The validator should be installed locally as part of your project using NPM:

```bash
$ npm install --save teslahtml-validator
```

Once installed, it can be imported into your project normally:

```typescript
import { TESLAValidator } from 'teslahtml-validator'

const markers = TESLAValidator.verify(html)
```

`.verify()` takes an html string and a ruleset and returns an array of objects in this format, intended for use by Monaco Editor:

```typescript
interface IMarker {
  code?: string
  resource?: string
  startColumn: number
  endColumn: number
  startLineNumber: number
  endLineNumber: number
  message: string
  severity: MarkerSeverity
  source: string
  owner: string
}
```

This is not a cheap process - it's strongly recommended to only run `.verify()` inside of a web worker, and to debounce the function that calls the worker.

### How it Works

There are two main pieces of the validator: the Parser, and the Reporter.

#### Parser

The parser takes a string of HTML and matches it against HTML and TESL regexes. On each capture, it records details about the match obtained from capture groups and saves these as a 'Block'. For example:

```html
<p style="color: red">
  Some text here
</p>
```

is turned into these blocks:

```typescript
{
  tagName: 'p',
  attributes: [
    {
      name: 'style',
      value: 'color: red',
      quote: '"',
      index: 0,
      raw: ' style="color: red"'
    }
  ],
  close: '',
  raw: '<p style="color: red">',
  position: 0,
  startLineNumber: 1,
  startColumn: 1,
  type: 'tagstart'
},
{
  raw: '\n  Some text here\n',
  position: 22,
  startLineNumber: 1,
  startColumn: 23,
  type: 'text'
},
{
  tagName: 'p',
  raw: '</p>',
  position: 40,
  startLineNumber: 3,
  startColumn: 1,
  type: 'tagend'
}
```

Note the 'type' property at the end of each object. Every time a block is saved, an event fires containing this Block Type. 

#### Reporter

The Reporter puts together the error messages that get returned to the application. It has easy to read functions designating marker severity that are used inside of the individual rules.

Available severities are:
* `info()`
* `warn()`
* `error()`

### Writing New Rules

Functionally speaking, all Rules are just event listeners, listening for a Block Type event, and acting conditionally based on the information that the block event has captured. All rules follow this format:

```typescript
interface IRule {
  id: string
  description: string
  init(parser: TESLAParser, reporter: TESLAReporter, options: unknown): void
}
```

The first thing to do when adding a new rule is to listen for the right Block-type. Let's say you want to ensure `<p>` tags never have `style` attributes on them. Since attributes are only found in the start tag, the block type event you'll use is `tagstart`:

```typescript
init(parser, reporter) {
  parser.addListener('tagstart', (event) => {
    // Rule content
  })
}
```

The event parameter gives you access to all of the information captured in that block - per the above format. To look for attributes, the parser provides an easy method for shallow checks of an event block's attributes. `getMapAttrs()` does not provide information like attribute index, so only use it if you are doing a check like this, where a tag should or should not have certain attributes.

```typescript
...
// Get the tag name of the event
const tagName = event.tagName.toLowerCase()
// Collect a shallow array of the attributes
const mapAttrs = parser.getMapAttrs(event.attributes)
...
```

Then, checking any `<p>` tag is as simple as writing a conditional check. Then the severity is decided by which reporter function is used. The position data passed through the Reporter is what allows Monaco editor to apply the squiggly markers visually indicating an error.

```typescript
// Check if the tag name is what we're looking for and if it also has the unwanted attribute
if (tagName === 'p' && ('style' in mapAttrs)) {
  reporter.warn(
    'Error Message',
    event.startColumn, // The Start Column
    event.startColumn + event.raw.length, // The End Column
    this, // The rule itself - sends information about the rule to the Reporter.
    event.raw.trim(), // The raw code implicated in the error
    event.startLineNumber, // The Start Line Number. An end line number following this is optional. If unset, as in this case, it uses the Start Line Number as the End Line Number.
  )
}
```

Once you have the rule, you need to add it to a ruleset in order for the validator to recognize it. Rulesets follow this format:

```typescript
interface IRuleset {
  'rule-id'?: boolean
  'rule-id2'?: boolean
  ...
}
```

In most cases, you'll want to use an existing ruleset as well as your custom rule. You can add it to a new ruleset object with the spread operator:

```typescript
const customRuleset = { ...TESLAValidator.emailHTMLRuleset, 'attr-p-nostyle': true }
```

Make sure the ID you used in the rule matches the ID you set in the Ruleset.

Once you have the ruleset created, you need to add your custom rule to the parser. The parser has a method provided for you to do this:

```typescript
import attrPNostyle from './attr-p-nostyle.js'
TESLAValidator.addRule(attrPNostyle)
```

Once it's been added to the parser and added to a ruleset, you can now run your code through the `.verify()` function with your custom ruleset as its second parameter. If you were to call the verify function on the above HTML with only this custom rule in the ruleset, you'd get this back:

```typescript
[
  {
    severity: 4,
    message: 'Error Message',
    code: '<p style="color: red">',
    resource: 'https://totalexpert.com/docs/user-guide/rules/attr-p-nostyle',
    startLineNumber: 1,
    endLineNumber: 1,
    startColumn: 1,
    endColumn: 23,
    source: 'attr-p-nostyle',
    owner: 'teslahtml-validator'
  }
]
```

You can then use this in Monaco Editor to create error markers.

#### Valid Events to Listen for

* `start`
* `tagstart`
* `tagend`
* `text`
* `comment`
* `outputTag`
* `startStatementTag`
* `endStatementTag`
* `end`